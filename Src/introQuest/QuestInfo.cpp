#include "stdafx.h"
#include "QuestInfo.h"

static CQuestInfo g_QuestInfo;

CQuestInfo::CQuestInfo(void)
{
	ExportAPIEntry(this);
	ExportDllMain();
}

CQuestInfo::~CQuestInfo(void)
{
}

void CQuestInfo::QueryNpc(std::vector<ST_NPC_INFO>& vecNPC)
{
	{
		ST_NPC_INFO npc;
		npc.id = 12;
		strcpy(npc.szName, "BOB 연구원");
		npc.x = 60;
		npc.y = 85;
		npc.w = 1;
		npc.h = 1;
		npc.patch = '@';
		strcpy(npc.szGreetMessage, "BOB에 합격한 걸 축하해!");
		vecNPC.push_back(npc);
	}

}

#include "ProfrogQuest.h"
#include "ProfrogSubQuest.h"
void CQuestInfo::QueryQuest(std::vector<ST_QUEST_DATA*>& vecQuest)
{
	vecQuest.push_back(new CProfrogQuest());
	vecQuest.push_back(new CProfrogSubQuest());
}
