#pragma once

#include "../QuestFramework/QuestFramework.h"

class CProfrogQuest : public ST_QUEST_DATA
{
	ST_QUEST_FLAG_FILTER m_ClearCondition;
public:
	CProfrogQuest(void);
	~CProfrogQuest(void);

	bool IsCleared(const ST_USER_QUESTINFO& info);
};
