#include "stdafx.h"
#include "ProfrogQuest.h"

CProfrogQuest::CProfrogQuest(void)
	: ST_QUEST_DATA()
{
	m_nTargetNpcId = 12;

	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "그럼 오리엔테이션은 이제 끝!"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "이제 질문을 받을건데 "));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "궁금한 게 없으면 해산~ "));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "궁금한 건 더 없니 ? "));

	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[12] = 0xFF;
		Reward.flags[12] = 0b10000000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[12] = 0xFF;
		Condition.flags[12] = 0b10000000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(0, " ... 저 궁금한게 있어요"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "그래? 뭐가 궁금하지? "));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "화장실 갔다오면서 봤는데요"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "비상계단 쪽에서 무슨 공사를 하는건가요?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, " 아 그거"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, " 용접하는 거란다."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "이제 안에서는 열리지 않을거야 "));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, " ...아까부터 112로 신고하는데 신호가 안가요. "));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, " 모바일 하이재킹이란다. "));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, " BOB에서 서류를 받을 때는 항상 조심하렴"));

	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[12] = 0xFF;
		Reward.flags[12] = 0b11000000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[12] = 0xFF;
		Condition.flags[12] = 0b11000000;	// ProfrogSubQuest
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "이건 미친 짓이야. 나는 여기서 나가겠어"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "왜 이런 일을 하는거죠?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "거기엔 슬픈 전설이 있어"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "그건 BoB 9기 때의 일이었지"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "우리는 교육생들에게 여러 시설을 제공하는 걸로 유명했어"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "하지만 2020년...코로나 사태로 센터 문을 닫게 되었지"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "결국 수료할때까지 센터에 오지 못한 교육생도 있었어"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "그러다보니 아쉽다는 불만들이 많이 접수되었고..."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "원장님께서는 이를 안타깝게 여겼지"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "그래서 한 가지 제안을 하게 되었단다."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "차라리 다음 기수부터는 처음부터 센터에 가둬놓고"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "수료할 때까지 나가지 못하게 하자는 거야"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "처음부터 출퇴근을 없애면 되는 문제였어"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "시설도 마음껏 이용할 수 있고 말이지"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "그럴듯한 제안이다보니"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "수료생들도 모두 동의했어"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "후배들을 생각하는 착한 마음이 만들어낸 기적이지"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "...이번에 합격한 제 의견은요?"));

	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[12] = 0xFF;
		Reward.flags[12] = 0b11100000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}
	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[12] = 0xFF;
		Condition.flags[12] = 0b11100000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(12, " 그럼 궁금한 건 더 없지? "));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "아 그리고 "));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "센터 내에는 수료생들의 지박령들이 남아있단다."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "후배들에게 전하고 싶은 말이 있는 것인지"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "아직도 센터를 맴돌고 있어"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "그들의 부탁을 들어주면 "));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "BoB 수료에 도움되는 여러 팁들을 받을 수 있어"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(12, "진짜 수료생들이 남긴 정보니 유용할거야"));

	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[12] = 0xFF;
		Reward.flags[12] = 0b11110000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[12] = 0xFF;
		Condition.flags[12] = 0b11110000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}


}

CProfrogQuest::~CProfrogQuest(void)
{
}
/*
bool CProfrogQuest::IsCleared(const ST_USER_QUESTINFO& info)
{
	return info.Check(m_ClearCondition);
}
*/