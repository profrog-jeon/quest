#include "stdafx.h"
#include "SeungMinQuest.h"

CSeungMinQuest::CSeungMinQuest(void)
	: ST_QUEST_DATA()
{
	m_nTargetNpcId = 14;

	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "...."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "...?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "...."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "じ..切軒岨 搾佃操..."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "食奄 姥汐精 鎧切軒暗窮...."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "嬢..嬢...!"));

	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[14] = 0xFF;
		Reward.flags[14] = 0b10000000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[14] = 0xFF;
		Condition.flags[14] = 0b10000000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "譲.... 煽奄....."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "......"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "巷充 析 赤嬢...?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "......"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "蟹.. 紫叔.. 引薦背醤 馬澗汽.."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "庁姥亜 蒸嬢辞.. 格巷 嬢形趨..."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "ばばばばばばばばばばばばばばば"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "....鎧亜 亀人匝猿...?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "舛源?!?!"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "益君檎 格巷 疏走!!!! ^~^"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "鎧 引薦岨 達焼辞 背�a "));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "!!!!"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "[左照薦念鯵降]号引 [朝凪砺軒焼]拭辞 引薦 3鯵 達焼辞 背�a ぞ"));

	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[14] = 0xFF;
		Reward.flags[14] = 0b11000000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[14] = 0xFF;
		Condition.flags[14] = 0b11111000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "板... 移酔 陥梅嬢... 食奄!!"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(引薦研 闇鎧早陥)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "神!!!!! 舛源 壱原趨 ばばばばばばばば"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "益掘亀 格 鉱拭 蒸陥 ばばばばばばば"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "悦汽 赤摂焼"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "引薦澗 什什稽 毘生稽 背鎧醤 税耕亜 赤澗暗醤."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "?? 焼艦;;"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "希 雌舌拝 呪 赤惟 企軒 引薦澗 走丞馬切."));

	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[14] = 0xFF;
		Reward.flags[14] = 0b11111100;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[14] = 0xFF;
		Condition.flags[14] = 0b11111100;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

}

CSeungMinQuest::~CSeungMinQuest(void)
{
}
