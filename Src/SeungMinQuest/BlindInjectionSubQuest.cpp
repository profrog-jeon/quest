#include "stdafx.h"
#include "BlindInjectionSubQuest.h"

CBlindInjectionSubQuest::CBlindInjectionSubQuest(void)
	: ST_QUEST_DATA()
{
	m_nTargetNpcId = 10022;

	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "刊浦亜税 引薦陥."));

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[14] = 0xFF;
		Condition.flags[14] |= 0b11000000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "戚闇 杭 引薦走..?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "[鷺虞昔球拭什泥燭昔詮芝]"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "げ..鷺虞.. 更..?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "[鷺虞昔球拭什泥燭昔詮芝]"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "人.. 戚闇 暁 更醤 せせ;;; ばばばば"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "杖献 背左切..."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(悪税切戟 左澗 掻)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(姥越元馬澗 掻)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(悪税切戟 左澗 掻)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(姥越元馬澗 掻)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(悪税切戟 左澗 掻)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(姥越元馬澗 掻)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(悪税切戟 左澗 掻)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(姥越元馬澗 掻)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "照鞠澗汽? せせせせせ"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(悪税切戟 左澗 掻)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(姥越元馬澗 掻)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(悪税切戟 左澗 掻)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(姥越元馬澗 掻)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "人 杭亜 吉 依 旭延廃汽... 杭亜 戚雌廃汽..?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "鎧 引薦亀 焼観汽 魁鎧醤走 せせせ.."));

	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[14] = 0xFF;
		Reward.flags[14] |= 0b00001000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[14] = 0xFF;
		Condition.flags[14] |= 0b00001000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "引薦研 魁蛎陥."));
}

CBlindInjectionSubQuest::~CBlindInjectionSubQuest(void)
{
}