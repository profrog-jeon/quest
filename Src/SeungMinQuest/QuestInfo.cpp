#include "stdafx.h"
#include "QuestInfo.h"

static CQuestInfo g_QuestInfo;

CQuestInfo::CQuestInfo(void)
{
	ExportAPIEntry(this);
	ExportDllMain();
}

CQuestInfo::~CQuestInfo(void)
{
}

void CQuestInfo::QueryNpc(std::vector<ST_NPC_INFO>& vecNPC)
{
	{
		ST_NPC_INFO npc;
		npc.id = 14;
		strcpy(npc.szName, "민승박");
		npc.x = 76;
		npc.y = 60;
		npc.w = 0.5;
		npc.h = 0.5;
		npc.patch = ']';
		strcpy(npc.szGreetMessage, "ㅎㅇㅎㅇ");
		vecNPC.push_back(npc);
	}

	{
		ST_NPC_INFO npc;
		npc.id = 10020;
		strcpy(npc.szName, "");
		npc.x = 108;
		npc.y = 63;
		npc.w = 0.5;  
		npc.h = 0.5;
		npc.patch = ']';
		strcpy(npc.szGreetMessage, "누군가의 과제다");
		vecNPC.push_back(npc);
	}
	{
		ST_NPC_INFO npc;
		npc.id = 10021;
		strcpy(npc.szName, "");
		npc.x = 238;
		npc.y = 157;
		npc.w = 0.5;
		npc.h = 0.5;
		npc.patch = ']';
		strcpy(npc.szGreetMessage, "누군가의 과제다");
		vecNPC.push_back(npc);
	}
	{
		ST_NPC_INFO npc;
		npc.id = 10022;
		strcpy(npc.szName, "");
		npc.x = 207;
		npc.y = 175;
		npc.w = 0.5;
		npc.h = 0.5;
		npc.patch = ']';
		strcpy(npc.szGreetMessage, "누군가의 과제다");
		vecNPC.push_back(npc);
	}
}

#include "SeungMinQuest.h"
#include "ARPSubQuest.h"
#include "HadoopSubQuest.h"
#include "BlindInjectionSubQuest.h"
void CQuestInfo::QueryQuest(std::vector<ST_QUEST_DATA*>& vecQuest)
{
	vecQuest.push_back(new CSeungMinQuest());
	vecQuest.push_back(new CARPQuest());
	vecQuest.push_back(new CHadoopSubQuest());
	vecQuest.push_back(new CBlindInjectionSubQuest());
}
