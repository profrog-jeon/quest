#include "stdafx.h"
#include "ARPSubQuest.h"

CARPQuest::CARPQuest(void)
	: ST_QUEST_DATA()
{
	m_nTargetNpcId = 10020;

	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "누군가의 과제다."));

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[14] = 0xFF;
		Condition.flags[14] |= 0b11000000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "아 뭐야 ARP 스푸핑 과제네..?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(과제를 열심히 한다..)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "와씨 과제 끝냈더니 해가 뜨네????"));
	
	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[14] = 0xFF;
		Reward.flags[14] |= 0b00100000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[14] = 0xFF;
		Condition.flags[14] |= 0b00100000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "과제를 끝냈다."));
}

CARPQuest::~CARPQuest(void)
{
}