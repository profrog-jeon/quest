#include "stdafx.h"
#include "HadoopSubQuest.h"

CHadoopSubQuest::CHadoopSubQuest(void)
	: ST_QUEST_DATA()
{
	m_nTargetNpcId = 10021;

	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "刊浦亜税 引薦陥."));

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[14] = 0xFF;
		Condition.flags[14] |= 0b11000000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "神 馬脚 竺帖 引薦醤??"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "竺帖 舛亀檎 襲走 せせせせせせせ"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(竺帖 掻..)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(7腰属 竺帖 掻..)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "せせ 岩蒸革 せせ"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "移酔 陥梅陥 せせ.."));

	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[14] = 0xFF;
		Reward.flags[14] |= 0b00010000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[14] = 0xFF;
		Condition.flags[14] |= 0b00010000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "引薦研 魁蛎陥."));
}

CHadoopSubQuest::~CHadoopSubQuest(void)
{
}