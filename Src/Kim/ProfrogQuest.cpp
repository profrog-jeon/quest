#include "stdafx.h"
#include "ProfrogQuest.h"

CProfrogQuest::CProfrogQuest(void)
	: ST_QUEST_DATA()
{
	m_nTargetNpcId = 10;

	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "집에 가고 싶다..."));
	
	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[1] = 0xFF;
		Reward.flags[1] = 0b11110000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}

	{
		// 이게 있어야 진짜 끝이다.
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[1] = 0xFF;
		Condition.flags[1] = 0b11110000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}
}

CProfrogQuest::~CProfrogQuest(void)
{
}
