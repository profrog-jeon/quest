#include "stdafx.h"
#include "QuestInfo.h"

static CQuestInfo g_QuestInfo;

CQuestInfo::CQuestInfo(void)
{
	ExportAPIEntry(this);
	ExportDllMain();
}

CQuestInfo::~CQuestInfo(void)
{
}

void CQuestInfo::QueryNpc(std::vector<ST_NPC_INFO>& vecNPC)
{
	{
		ST_NPC_INFO npc;
		npc.id = 4;
		strcpy(npc.szName, "우병수");
		npc.x = 190;
		npc.y = 100;
		npc.w = 1;
		npc.h = 1;
		npc.patch = '@';
		strcpy(npc.szGreetMessage, "다 내꺼야, 감자 튀김...");
		vecNPC.push_back(npc);
	}

	{
		ST_NPC_INFO npc;
		npc.id = 10006;
		strcpy(npc.szName, "프로젝트집");
		npc.x = 225;
		npc.y = 179;
		npc.w = 0.5;
		npc.h = 0.5;
		npc.patch = '^';
		strcpy(npc.szGreetMessage, "읽기 싫을정도로 두꺼운 책이다.");
		vecNPC.push_back(npc);
	}

	{
		ST_NPC_INFO npc;
		npc.id = 10007;
		strcpy(npc.szName, "연구원님");
		npc.x = 230;
		npc.y = 173;
		npc.w = 0.5;
		npc.h = 0.5;
		npc.patch = '&';
		strcpy(npc.szGreetMessage, "BoB는 차세대 보안 리더를 양성하는 곳입니다.");
		vecNPC.push_back(npc);
	}
}

#include "WBSQuest.h"
#include "WBSSubQuest.h"
void CQuestInfo::QueryQuest(std::vector<ST_QUEST_DATA*>& vecQuest)
{
	vecQuest.push_back(new CWBSQuest());
	vecQuest.push_back(new CWBSSubQuest());
}
