#include "stdafx.h"
#include "WBSSubQuest.h"

CWBSSubQuest::CWBSSubQuest(void)
	: ST_QUEST_DATA()
{
	m_nTargetNpcId = 10006;

	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "읽기 싫을정도로 두꺼운 책이다."));

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[4] = 0xFF;
		Condition.flags[4] = 0b11000000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "두꺼운 책이 책장에 꽂혀있다."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "[프로젝트집]을 발견했다."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(10007, "누구냐 넌. 용건이 뭐지?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "아, 선배님들의 프로젝트를 참고하려고요."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(10007, "프로젝트집을 마음대로 보는 것은 허락할 수 없어."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(머릿속이 혼미해진다.)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(어떻게 해야 이걸 가져갈 수 있을까.)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(그 때 책을 흘끗 봤다. 한줄기 빛이 보인다.)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "연구원님, 여기 결과보고서 보니까"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "본 보고서는 향후 BoB 멘토단, 수료생, 차 기수 교육생에게"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "정보를 제공하기 위한 목적으로 활용된다고 써져있네요."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "저는 정보를 제공받고 싶습니다."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(10007, "(크윽, 간.파. 당했군.)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(10007, "그래. 그 열정 앞으로도 계속 유지하도록."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(10007, "잘 참고하도록."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "감사합니다."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "[프로젝트집]을 획득했다."));
	
	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[4] = 0xFF;
		Reward.flags[4] = 0b11100000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}
}

CWBSSubQuest::~CWBSSubQuest(void)
{
}
