#include "stdafx.h"
#include "WBSQuest.h"

CWBSQuest::CWBSQuest(void)
	: ST_QUEST_DATA()
{
	m_nTargetNpcId = 4;

	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "안녕하신가! 힘세고 강한 아침,"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "만일 내게 물어보면 나는 병수."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "취약점분석트랙 9기 수강생이다."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(이 사람 왈도체가 심각하다.)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "프로젝트 팁이 궁금하면 연락하라고."));

	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[m_nTargetNpcId] = 0xFF;
		Reward.flags[m_nTargetNpcId] = 0b10000000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[m_nTargetNpcId] = 0xFF;
		Condition.flags[m_nTargetNpcId] = 0b10000000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "저도 프로젝트하면서 굴러야하는데요"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "어떻게 하면 top 30에 갈 수 있을까요?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "난 top 30을 못갔기 때문에 그건 말 못해주지만"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "그래도 프로젝트에 도움이 되는 팁을 줄게."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(신뢰성이 뚝 떨어진다.)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "못미더워도 잘 들어. 정말 도움이 될 수 있어."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "BoB센터에는 이전 기수 선배들의 프로젝트 모음집이 있어."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "그걸 참고하면 도움이 되겠지?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "아마 좌표(70, 70) 쯤에 있을거야."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "행운을 빈다구, 이녀석."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(뭔가 신뢰성이 다시 올라간 느낌이다.)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "책을 얻는데 획득하면 나한테와서 알려줘"));

	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[m_nTargetNpcId] = 0xFF;
		Reward.flags[m_nTargetNpcId] = 0b11000000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[m_nTargetNpcId] = 0xFF;
		Condition.flags[m_nTargetNpcId] = 0b11100000;	// WBSSubQuest
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "여기 가져왔습니다."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "오, 연구원님이 지키고있었을텐데,"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "가져오는데 성공했구나. 정말 대단해."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "프로젝트 성공을 기원한다."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "[프로젝트에 대한 자신감]을 획득했다."));

	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[m_nTargetNpcId] = 0xFF;
		Reward.flags[m_nTargetNpcId] = 0b11110000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}
	
	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[m_nTargetNpcId] = 0xFF;
		Condition.flags[m_nTargetNpcId] = 0b11110000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}
}

CWBSQuest::~CWBSQuest(void)
{
}
