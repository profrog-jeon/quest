#pragma once

#include "../QuestFramework/QuestFramework.h"

class PressPlateX : public ST_QUEST_DATA
{
public:
	PressPlateX(void);
	~PressPlateX(void);

	bool IsCleared(const ST_USER_QUESTINFO& info);
};

