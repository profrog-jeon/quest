#pragma once

#include "../QuestFramework/QuestFramework.h"

class PressPlateO : public ST_QUEST_DATA
{
public:
	PressPlateO(void);
	~PressPlateO(void);

	bool IsCleared(const ST_USER_QUESTINFO& info);
};

