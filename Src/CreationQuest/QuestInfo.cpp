#include "stdafx.h"
#include "QuestInfo.h"

static CQuestInfo g_QuestInfo;

CQuestInfo::CQuestInfo(void)
{
	ExportAPIEntry(this);
	ExportDllMain();
}

CQuestInfo::~CQuestInfo(void)
{
}

void CQuestInfo::QueryNpc(std::vector<ST_NPC_INFO>& vecNPC)
{
	{
		ST_NPC_INFO npc;
		npc.id = 11;
		strcpy(npc.szName, "창조");
		npc.x = 85;
		npc.y = 70;
		npc.w = 1;
		npc.h = 1;
		npc.patch = '@';
		strcpy(npc.szGreetMessage, "안녕 ㅎ");
		vecNPC.push_back(npc);
	}

	{
		ST_NPC_INFO npc;
		npc.id = 10016;
		strcpy(npc.szName, "LG그램");
		npc.x = 201;
		npc.y = 177;
		npc.w = 0.5;
		npc.h = 0.5;
		npc.patch = 'L';
		strcpy(npc.szGreetMessage, "LG그램... 누가 여기서 이걸 잃어버렸지?");
		vecNPC.push_back(npc);
	}

	/*
	{
		ST_NPC_INFO npc;
		npc.id = 10017;
		strcpy(npc.szName, "신분당선상현역장");
		npc.x = 250;
		npc.y = 0;
		npc.w = 0.5;
		npc.h = 0.5;
		npc.patch = 'S';
		strcpy(npc.szGreetMessage, "오늘도 신분당선을 이용해주셔서 고맙습니다.");
		vecNPC.push_back(npc);
	}
	*/
}

#include "CreationQuest.h"
#include "CreationSubItem.h"

void CQuestInfo::QueryQuest(std::vector<ST_QUEST_DATA*>& vecQuest)
{
	vecQuest.push_back(new CCreationQuest());
	vecQuest.push_back(new CCreationSubItem());
}
