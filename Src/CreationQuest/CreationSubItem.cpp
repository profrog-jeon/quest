#include "CreationSubItem.h"

CCreationSubItem::CCreationSubItem(void)
	: ST_QUEST_DATA()
{
	m_nTargetNpcId = 10016;

	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(카페테리아에 LG그램이 놓여져 있군.)"));

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[NPC_ID] = 0xFF;
		Condition.flags[NPC_ID] = 0b10000000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "LG그램 찾았다!"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "이제 창조한테 갖다주면 될듯. 돌아가자."));

	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[NPC_ID] = 0xFF;
		Reward.flags[NPC_ID] = 0b11100000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}
}

CCreationSubItem::~CCreationSubItem(void)
{
}
