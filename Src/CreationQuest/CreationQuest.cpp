#include "stdafx.h"
#include "CreationQuest.h"

CCreationQuest::CCreationQuest(void)
	: ST_QUEST_DATA()
{
	m_nTargetNpcId = 11;

	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "안녕. 나 창조인데"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "BoB 센터 어딘가에 노트북을 잃어버린 것 같아."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "ㄷㄷ"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "카페테리아 근처 어딘가에 잃어버려서"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "거기까지 가야 하는데..."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "제발 부탁한다. (201, 177) 쯤에 한번 가봐."));

	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[m_nTargetNpcId] = 0xFF;
		Reward.flags[m_nTargetNpcId] = 0b10000000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[m_nTargetNpcId] = 0xFF;
		Condition.flags[m_nTargetNpcId] = 0b11100000;	// CreationSubQuest
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "혹시 이건가요?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(LG그램을 건네준다)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "아닛?! 진짜로 찾아줘서 고마워~"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "그래서 보답으로 기프티콘 줄게."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "[기프티콘]을 습득했다."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "BoB 생활 포기하지 말고 끝까지 화이팅하자 ^^"));

	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[m_nTargetNpcId] = 0xFF;
		Reward.flags[m_nTargetNpcId] = 0b11110000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[m_nTargetNpcId] = 0xFF;
		Condition.flags[m_nTargetNpcId] = 0b11110000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}
}

CCreationQuest::~CCreationQuest(void)
{
}
