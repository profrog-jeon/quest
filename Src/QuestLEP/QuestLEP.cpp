#include "stdafx.h"
#include "QuestLEP.h"

CProfrogQuest::CProfrogQuest(void)
	: ST_QUEST_DATA()
{
	m_nTargetNpcId = 2;

	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "나에 대한 정보는 BoB wiki를 참조해~"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "... 안물어봤는데?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, ";; 너 BoB wiki 테러 당하고 싶니?"));

	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[2] = 0xFF;
		Reward.flags[2] = 0b10000000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[2] = 0xFF;
		Condition.flags[2] = 0b10000000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "아 C++ 잘하고 싶다.........."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "한권 보면 척하고 고수가 될만한 책은 없을까?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "[크로스플랫폼 핵심모듈 설계의 기술 책]은 어때? 이 책 좋대"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "엇 그럼 그 책 한 권 구해줄 수 있어?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "내가 지금 돈이 없거든...."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "[전상현 멘토]님께 가서 책 한 권 얻어오면 나도 좋은 걸 줄게."));

	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[2] = 0xFF;
		Reward.flags[2] = 0b11000000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[1] = 0xFF;
		Condition.flags[1] = 0b11110000;	// ProfrogSubQuest
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "이 책 맞지?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "(책을 건네준다)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "엇 맞아 고마워 ㅠㅠ 이제 난 C++의 고수야."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "난 이제 다 봤던 책이 있었는데 이거 답례로 줄게"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "[윈도우즈 API 정복 1권]을 습득했다."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "책 잘 볼게 고마워~"));

	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[2] = 0xFF;
		Reward.flags[2] = 0b11110000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}
}

CProfrogQuest::~CProfrogQuest(void)
{
}
