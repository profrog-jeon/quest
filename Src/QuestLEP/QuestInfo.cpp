#include "stdafx.h"
#include "QuestInfo.h"

static CQuestInfo g_QuestInfo;

CQuestInfo::CQuestInfo(void)
{
	ExportAPIEntry(this);
	ExportDllMain();
}

CQuestInfo::~CQuestInfo(void)
{
}

void CQuestInfo::QueryNpc(std::vector<ST_NPC_INFO>& vecNPC)
{
	{
		ST_NPC_INFO npc;
		npc.id = 2;
		strcpy(npc.szName, "이은표");
		npc.x = 90;
		npc.y = 115;
		npc.w = 0.5;
		npc.h = 0.5;
		npc.patch = '@';
		strcpy(npc.szGreetMessage, "내가 이 게임의 GDI를 개발했어.");
		vecNPC.push_back(npc);
	}
}

#include "QuestLEP.h"
#include "QuestLEP.h"
void CQuestInfo::QueryQuest(std::vector<ST_QUEST_DATA*>& vecQuest)
{
	vecQuest.push_back(new CProfrogQuest());
}
