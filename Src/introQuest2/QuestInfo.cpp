#include "stdafx.h"
#include "QuestInfo.h"

static CQuestInfo g_QuestInfo;

CQuestInfo::CQuestInfo(void)
{
	ExportAPIEntry(this);
	ExportDllMain();
}

CQuestInfo::~CQuestInfo(void)
{
}

void CQuestInfo::QueryNpc(std::vector<ST_NPC_INFO>& vecNPC)
{
	{
		ST_NPC_INFO npc;
		npc.id = 13;
		strcpy(npc.szName, "안내판");
		npc.x = 57;
		npc.y = 105;
		npc.w = 1;
		npc.h = 1;
		npc.patch = '@';
		strcpy(npc.szGreetMessage, "(안내판이 보인다.)");
		vecNPC.push_back(npc);
	}

	{
		ST_NPC_INFO npc;
		npc.id = 10013;
		strcpy(npc.szName, "지박령 멘티");
		npc.x = 66;
		npc.y = 116;
		npc.w = 1;
		npc.h = 1;
		npc.patch = '@';
		strcpy(npc.szGreetMessage, "오 새로운 기수인가");
		vecNPC.push_back(npc);
	}
}

#include "ProfrogQuest.h"
#include "ProfrogSubQuest.h"
void CQuestInfo::QueryQuest(std::vector<ST_QUEST_DATA*>& vecQuest)
{
	vecQuest.push_back(new CProfrogQuest());
	vecQuest.push_back(new CProfrogSubQuest());
}
