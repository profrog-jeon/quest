#include "stdafx.h"
#include "ProfrogQuest.h"

CProfrogQuest::CProfrogQuest(void)
	: ST_QUEST_DATA()
{
	m_nTargetNpcId = 13;

	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "저 문지기는 어떻게 해야하는 거지...?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "어 안내판에 무언가 써져 있다."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(13, "(BoB에서는 보통 대화로 해결합니다.)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(13, "(하지만 세상 모든 일이 대화로 해결되지는 않죠.) "));
	m_Sequence.push_back(ST_QUEST_MESSAGE(13, "(그럴때야 말로 총을 써야 합니다.)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(13, "(총은 훌륭한 대화수단입니다.)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(13, "(스페이스바를 누르면 총을 쏠 수 있습니다.)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(13, "(그들에게 당신이 누구인지 보여줍시다.)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "...이게 BoB 방식인가"));


	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[13] = 0xFF;
		Reward.flags[13] = 0b11000000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}

	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[13] = 0xFF;
		Condition.flags[13] = 0b11100000;	// ProfrogSubQuest
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "어 안내판에 새로운 글이 써져있어"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(13, "(BoB는 매년 똑같습니다.)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(13, "(매년 새로운 교육생들이 들어오고,)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(13, "(그들 중 일부는 새로운 지박령이 됩니다.)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(13, "(그리고 다시 새로운 기수들을 기다립니다.)"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(13, "(당신은 지금 몇 기입니까?)"));

	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[13] = 0xFF;
		Reward.flags[13] = 0b11110000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}

		{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[13] = 0xFF;
		Condition.flags[13] = 0b11110000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}


}

CProfrogQuest::~CProfrogQuest(void)
{
}

/*
bool CProfrogQuest::IsCleared(const ST_QUEST_DATA& info)
{
	//return info.Check(m_ClearCondition);
}
*/
