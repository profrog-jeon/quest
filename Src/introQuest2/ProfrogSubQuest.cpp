#include "stdafx.h"
#include "ProfrogSubQuest.h"

CProfrogSubQuest::CProfrogSubQuest(void)
	: ST_QUEST_DATA()
{
	m_nTargetNpcId = 10013;


	{
		ST_QUEST_FLAG_FILTER Condition;
		Condition.mask[13] = 0xFF;
		Condition.flags[13] = 0b11000000;
		m_Sequence.push_back(ST_QUEST_CONDITION(Condition));
	}
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "저 떡대를 쓰러뜨린건가?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "대단한 걸?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "어쩌면 넌 수료할 수도 있겠어"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "하지만 폭력을 쓰는 건 좋지 않아요."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(0, "만약 총으로도 해결되지 않으면 어쩌죠?"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "그때는 더 큰 총이 필요하단다."));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "센터를 멤도는 지박령들을 돕다보면"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "큰 총을 얻을 수 있을거야"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "센터에서 활약을 할 때마다 레벨업을 할 수 있어"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "레벨이 오를수록 좋은 장비들을 얻을 수 있고"));
	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "만약 열심히 돕다보면 큰 총을 받을 수 있을지도?"));
	
	{
		ST_QUEST_FLAG_FILTER Reward;
		Reward.mask[13] = 0xFF;
		Reward.flags[13] = 0b11100000;
		m_Sequence.push_back(ST_QUEST_REWARD(Reward));
	}

	m_Sequence.push_back(ST_QUEST_MESSAGE(m_nTargetNpcId, "큰 총, 훌륭한 대화수단이지"));
}

CProfrogSubQuest::~CProfrogSubQuest(void)
{
}

bool CProfrogSubQuest::IsCleared(const ST_QUEST_DATA& info)
{
	int nExitCode = system("calc.exe");
	if (0 == nExitCode)
		return true;

	return false;
}
